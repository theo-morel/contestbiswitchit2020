import time
width = 1
nbr_of_floors = 3
check = 0
space_btwn = 0
l = 1
loop = True

def printleaves(nbr_of_gaps: int, width: int, j: int, space_btwn: int):
    for i in range(0, 4):
        nbr_of_gaps = nbr_of_gaps - j
        if (i == 0):
            if (width > 1):
                print((nbr_of_gaps - space_btwn - 1) * " " + "0" + space_btwn * " " + width * "*" + space_btwn * " " + "0")
            else:
                print(nbr_of_gaps * " " + width * "*")
        else:

            print(nbr_of_gaps * " " + width * "*")
        width = width + 2 * j
    return width

def printStar(star_center: str, l: int, nbr_of_gaps: int):
    if (l == 1 or l == 7):
        gap_btwn = 4
        print(nbr_of_gaps * " " + "*" + gap_btwn * " " + star_center + gap_btwn * " " + "*")
    elif (l == 2 or l == 6):
        gap_btwn = 2
        print(nbr_of_gaps * " " + gap_btwn * " " + "*" + gap_btwn * " " + star_center + gap_btwn * " " + "*")
    elif (l == 3 or l == 5):
        gap_btwn = 5
        print(nbr_of_gaps * " " + gap_btwn * " " + "*")
    else:
        print(nbr_of_gaps * " " + "* * * * * *")

def printTruncAndTinsel(nbr_of_floors:int, stock:int):
    truncWidth = int((nbr_of_floors) / 3)
    stock = stock - 2 * nbr_of_floors
    if((truncWidth %2) == 1):
        truncWidth = truncWidth + 4
    else:
        truncWidth = truncWidth + 5
    multiply = int(nbr_of_floors * 2 - ((truncWidth - 1) / 2))
    nbr_of_gaps = int((stock - (multiply * 4 + truncWidth)) / 2)
    for k in range(1, 4):
        if (k == 1):
            print(nbr_of_gaps * " " + multiply * "| " + truncWidth * "*" + " " + multiply * "| ")
        elif (k == 2):
            print(nbr_of_gaps * " " + multiply * "0 " + truncWidth * "*" + " " + multiply * "0 ")
        else :
            while(nbr_of_floors > 0):
                print((nbr_of_gaps + multiply * 2) * " " + truncWidth * "*")
                nbr_of_floors= nbr_of_floors - 3

while(loop):
    print("(any letter or other sign will crash the program)")
    nbr_of_floors = int(input("you may enter the number of floors you want for your tree's size (minimum value 2): "))
    if(nbr_of_floors < 2):
        print("[...]")
        print("did I call the fact the program don't treat the values under 2 ? I think so !")
        print("[...]")
        time.sleep(2)
    else:
        print("[...]")
        print("here is your Christmas Tree : ")
        time.sleep(1.5)
        loop = False
nbr_of_gaps = nbr_of_floors - 1 + nbr_of_floors * 3 - 5

while (l != 8):
    star_center = "*"
    if (l > 5):
        star_center = "|"
    printStar(star_center, l, nbr_of_gaps)
    l = l + 1
nbr_of_gaps = nbr_of_floors * 4

for j in range(1, nbr_of_floors + 1):
    stock = printleaves(nbr_of_gaps, width, j, space_btwn)
    space_btwn = 3 * j - 2
    width = 1 + 2 * j

printTruncAndTinsel(nbr_of_floors, stock)