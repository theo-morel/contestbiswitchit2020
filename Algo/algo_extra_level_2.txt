width, nbr_of_floor, check en tant qu'entiers
loop en tant que booléen
width <- 1
nbr_of_floor <- 3
check <- 0
loop <- vrai

printStar(star_center, l, nbr_of_gaps)(
	si (l == 1 ou l == 7) alors :
		gap_btwn <- 4
		afficher (nbr_of_gaps * " ", "*", gap_btwn * " ", star_center, gap_btwn * " ", "*")

 	sinon si (l == 2 ou l == 6) alors :
        	gap_btwn <- 2
        	afficher (nbr_of_gaps * " ", gap_btwn * " ", "*", gap_btwn * " ", star_center, gap_btwn * " ", "*")

   	sinon si (l == 3 ou l == 5) alors :
		gap_btwn <- 5
   		afficher (nbr_of_gaps * " ", gap_btwn * " ", "*")
	sinon
        	afficher (nbr_of_gaps * " ", "* * * * * *")
	fin si
)

printStar_c(star_center, l, nbr_of_gaps)(
	si l == 1 ou l == 7 alors :
		gap_btwn <- 4
		afficher (nbr_of_gaps * " ", "*", gap_btwn * " ", star_center, gap_btwn * " ", "*" , (2 * nbr_of_gaps + 1 ) * " ", "*", gap_btwn * " ", star_center, gap_btwn * " ", "*")
 	sinon si l == 2 ou l == 6 alors :
        	gap_btwn <- 2
		nbr_of_gaps2 <- nbr_of_gaps + gap_btwn + 1
        	afficher (nbr_of_gaps * " ", gap_btwn * " ", "*", gap_btwn * " ", star_center, gap_btwn * " ", "*", (nbr_of_gaps + nbr_of_gaps2) * " ", gap_btwn * " ", "*", gap_btwn * " ", star_center + gap_btwn * " ", "*")


   	sinon si (l == 3 ou l == 5) alors :
		gap_btwn <- 5
   		afficher (nbr_of_gaps * " ", gap_btwn * " ", "*")
	sinon
        	afficher (nbr_of_gaps * " ", "* * * * * *")
	fin si
)

printLeaves(nbr_of_gaps, width, j, space_btwn)(
	Pour i allant de 0 à 4
		nbr_of_gaps <- nbr_of_gaps - j
		si i == 0 alors :
			si width > 1 alors :
				afficher ((nbr_of_gaps - space_btwn - 1) * " ", "0", space_btwn*" ", width * "*", space_btwn * " ", "0")
			sinon
				afficher (nbr_of_gaps * " ", width * "*")
			fin si
		sinon :
			afficher (nbr_of_gaps * " ", width * "*"
		fin si
		width <- width+ 2 * j
	Fin Pour
	return width
)



printTruncAndTinsel(nbr_of_floors, stock)(
	truncWidth <- (nbr_of_floors / 3)
	stock <- stock - 2 * nbr_of_floors
	si (truncWidth %2) == 1 alors :
		truncWidth <- truncWidth + 4
	sinon
		truncWidth <- truncWidth + 5
	fin si
	multiply <- nbr_of_floors * 2 - ((truncWidth - 1)/2)
	nbr_of_gaps <- stock - ((multiply * 4 + truncWidth)/2)

	Pour k allant de 1 à 4
		si k == 1 alors :
			afficher (nbr_of_gaps * " ", multiply * "| ", 5 * "*", " ", multiply * "| ")
		sinon si k == 2 alors :
			afficher (nbr_of_gaps * " ", multiply * "0 ", 5 * "*", " ", multiply * "0 ")
		sinon
			tant que nbr_of_floors > 0 alors :
				afficher ((nbr_of_gaps + multiply * 2) * " ", 5 * "*")
				nbr_of_floors <- nbr_of_floors - 3
			fin tant que
		fin si
	Fin Pour



)
Début

tant que loop :
	afficher ("Any letter or other sign will crash the program")
	afficher ("You may enter the number of floors you want for your tree's size :")
	lire nbr_of_floors
	int (nbr_of_floors)
	
	si nbr_of_floors > 2 alors :
		afficher ("[...]")
       	 	afficher ("did I call the fact the program don't treat the values under 2 ? I think so !")
       		afficher ("[...]")
	sinon
		afficher ("[...]")
      		afficher ("here is your Christmas Tree : ")
     		loop <- False
	fin si
Fin Tant Que

nbr_of_gaps <- nbr_of_floors - 1 + nbr_of_floors * 3 - 5

l <- 1
tant que l <> 8 alors :
	star_center <- "*"
	si l > 5 alors :
		star_center = "|"
	fin si
	printStar(star_center, l, nbr_of_gaps)
	l <- l + 1
Fin tant que

nbr_of_gaps = nbr_of_floors * 4

space_btwn <- 0
Pour j allant de 1 à nbr_of_floors + 1 :
	stock <- printLeaves(nbr_of_gaps, width, j, space_btwn)
	space_btwn <- (3 * j - 2)
	width <- (1 + 2 * j)

Fin Pour

printTruncAndTinsel(nbr_of_floors)

Fin


