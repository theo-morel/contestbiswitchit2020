width <- 1
nbr_of_floors <- 3
nbr_of_gaps <- 16

printLine(nbr_of_gaps, width, j)(
	Pour i allant de 0 à 4
		nbr_of_gaps <- nbr_of_gaps - j
		afficher (nbr_of_gaps * " ", width * "*")
		width <- width + 2 * j
	fin Pour
)
Début

	Pour j allant de 1 à nbr_of_floors + 1
		printline(nbr_of_gaps, width, j)
		width <- 1 + 2 * j
	fin Pour

	Pour k allant de 1 à 4
		afficher (" " * 12, "*" * 5)
	fin Pour
Fin