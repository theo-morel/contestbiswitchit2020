import time
width = 1
nbr_of_floors = 3
nbr_of_gaps = 16
def printline(nbr_of_gaps:int, width:int, j:int):
    for i in range(0, 4):
        nbr_of_gaps = nbr_of_gaps - j
        print(nbr_of_gaps * " " + width * "*")
        width = width + 2 * j
        time.sleep(0.3)


for j in range(1, nbr_of_floors + 1):
    printline(nbr_of_gaps, width, j)
    width = 1 + 2 * j
    time.sleep(0.3)




