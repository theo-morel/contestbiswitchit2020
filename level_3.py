import time
width = 1
nbr_of_floors = 3
nbr_of_gaps = 16

def printline(nbr_of_gaps:int, width:int, j:int):
    for i in range(0, 4):
        nbr_of_gaps = nbr_of_gaps - j
        print(nbr_of_gaps * " " + width * "*")
        width = width + 2 * j
        time.sleep(0.3)

for j in range(1, nbr_of_floors + 1):
    printline(nbr_of_gaps, width, j)
    width = 1 + 2 * j

for k in range(1, 4):
    part_1 = " | | | | "
    part_2 = " 0 0 0 0 "
    if(k == 1):
        print(4 * " " + part_1 + 5 * "*" + part_1)
        time.sleep(0.3)

    elif(k == 2):
        print(4 * " " + part_2 + 5 * "*" + part_2)
        time.sleep(0.3)
    else:
        print(13 * " " + 5 * "*")