import time
width = 1
nbr_of_floors = 3
check = 0
space_btwn = 0
l = 1
loop = True

def printStar(star_center: str, l: int, nbr_of_gaps: int):
    if (l == 1 or l == 7):
        gap_btwn = 4
        print(nbr_of_gaps * " " + "*" + gap_btwn * " " + star_center + gap_btwn * " " + "*")
    elif (l == 2 or l == 6):
        gap_btwn = 2
        print(nbr_of_gaps * " " + gap_btwn * " " + "*" + gap_btwn * " " + star_center + gap_btwn * " " + "*")
    elif (l == 3 or l == 5):
        gap_btwn = 5
        print(nbr_of_gaps * " " + gap_btwn * " " + "*")
    else:
        print(nbr_of_gaps * " " + "* * * * * *")

def printStar_c(star_center: str, l: int, nbr_of_gaps: int):
    if (l == 1 or l == 7):
        gap_btwn = 4
        print(nbr_of_gaps * " " + "*" + gap_btwn * " " + star_center + gap_btwn * " " + "*" + (2 * nbr_of_gaps + 1 ) * " " + "*" + gap_btwn * " " + star_center + gap_btwn * " " + "*")
    elif (l == 2 or l == 6):
        gap_btwn = 2
        nbr_of_gaps2 = nbr_of_gaps + gap_btwn +1
        print(nbr_of_gaps * " " + gap_btwn * " " + "*" + gap_btwn * " " + star_center + gap_btwn * " " + "*" + (nbr_of_gaps + nbr_of_gaps2) * " " + gap_btwn * " " + "*" + gap_btwn * " " + star_center + gap_btwn * " " + "*")
    elif (l == 3 or l == 5):
        gap_btwn = 5
        nbr_of_gaps2 = nbr_of_gaps + gap_btwn +1
        print(nbr_of_gaps * " " + gap_btwn * " " + "*" + (nbr_of_gaps + nbr_of_gaps2) * " " + gap_btwn * " " + "*")
    else:
        print(nbr_of_gaps * " " + "* * * * * *" + (2 * nbr_of_gaps + 1) * " " + "* * * * * *")

def printleaves(nbr_of_gaps: int, width: int, j: int, space_btwn: int):
    for i in range(0, 4):
        nbr_of_gaps = nbr_of_gaps - j
        if (i == 0):
            if (width > 1):
                print((nbr_of_gaps - space_btwn - 1) * " " + "0" + space_btwn * " " + width * "*" + space_btwn * " " + "0")
            else:
                print(nbr_of_gaps * " " + width * "*")
        else:

            print(nbr_of_gaps * " " + width * "*")
        width = width + 2 * j
    return width

def printleaves_c(nbr_of_gaps: int, width: int, j: int, space_btwn: int):
    for i in range(0, 4):
        nbr_of_gaps = nbr_of_gaps - j
        if (i == 0):
            if (width > 1):
                print((nbr_of_gaps - space_btwn - 1) * " " + "0" + space_btwn * " " + width * "*" + space_btwn * " " + "0" + ( 1 + 2 * (nbr_of_gaps - space_btwn - 1)) * " " + "0" + space_btwn * " " + width * "*" + space_btwn * " " + "0")
            else:
                print(nbr_of_gaps * " " + width * "*" + (1 + 2 * nbr_of_gaps) * " " + width * "*")
        else:

            print(nbr_of_gaps * " " + width * "*" + (1 + 2 * nbr_of_gaps) * " " + width * "*")
        width = width + 2 * j
    return width

def printTruncAndTinsel(nbr_of_floors:int, stock:int):
    truncWidth = int((nbr_of_floors) / 3)
    stock = stock - 2 * nbr_of_floors
    if((truncWidth %2) == 1):
        truncWidth = truncWidth + 4
    else:
        truncWidth = truncWidth + 5
    multiply = int(nbr_of_floors * 2 - ((truncWidth - 1) / 2))
    nbr_of_gaps = int((stock - (multiply * 4 + truncWidth)) / 2)
    for k in range(1, 4):
        if (k == 1):
            print(nbr_of_gaps * " " + multiply * "| " + truncWidth * "*" + " " + multiply * "| ")
        elif (k == 2):
            print(nbr_of_gaps * " " + multiply * "0 " + truncWidth * "*" + " " + multiply * "0 ")
        else :
            while(nbr_of_floors > 0):
                print((nbr_of_gaps + multiply * 2) * " " + truncWidth * "*")
                nbr_of_floors= nbr_of_floors - 3

def printTruncAndTinsel_c(nbr_of_floors:int, stock:int):
    truncWidth = int((nbr_of_floors) / 3)
    stock = stock - 2 * nbr_of_floors
    if((truncWidth %2) == 1):
        truncWidth = truncWidth + 4
    else:
        truncWidth = truncWidth + 5
    multiply = int(nbr_of_floors * 2 - ((truncWidth - 1) / 2))
    nbr_of_gaps = int((stock - (multiply * 4 + truncWidth)) / 2)
    for k in range(1, 4):
        if (k == 1):
            print(nbr_of_gaps * " " + multiply * "| " + truncWidth * "*" + " " + multiply * "| " + 2 * nbr_of_gaps * " " + multiply * "| " + truncWidth * "*" + " " + multiply * "| ")
        elif (k == 2):
            print(nbr_of_gaps * " " + multiply * "0 " + truncWidth * "*" + " " + multiply * "0 " + 2 * nbr_of_gaps * " " + multiply * "0 " + truncWidth * "*" + " " + multiply * "0 ")
        else :
            while(nbr_of_floors > 0):
                print((nbr_of_gaps + multiply * 2) * " " + truncWidth * "*" + (1 + 2 * (nbr_of_gaps + multiply * 2)) * " " + truncWidth * "*")
                nbr_of_floors= nbr_of_floors - 3

while(loop):
    print("(any letter or other sign will crash the program)")
    nbr_of_floors = int(input("you may enter the number of floors you want for your tree's size (minimum value 2): "))
    if(nbr_of_floors < 2):
        print("[...]")
        print("did I call the fact the program don't treat the values under 2 ? I think so !")
        print("[...]")
        time.sleep(2)
    else:
        while (loop):
            clone = str(input("would you like to clone your tree ? y/n : "))
            if(clone == "y" or clone == "n"):
                print("[...]")
                print("here is your Christmas Tree : ")
                loop = False
            else:
                print("[...]")
                print("mate, you must enter y or n . Nothing else !")
                print("[...]")
                time.sleep(1.5)
    time.sleep(1.5)

nbr_of_gaps = nbr_of_floors - 1 + nbr_of_floors * 3 - 5

while (l != 8):
    star_center = "*"
    if (l > 5):
        star_center = "|"
    if(clone == "y"):
        printStar_c(star_center, l, nbr_of_gaps)
    else:
        printStar(star_center, l, nbr_of_gaps)
    l = l + 1
nbr_of_gaps = nbr_of_floors * 4

for j in range(1, nbr_of_floors + 1):
    if(clone == "y"):
        stock = printleaves_c(nbr_of_gaps, width, j, space_btwn)
    else:
        stock = printleaves(nbr_of_gaps, width, j, space_btwn)
    space_btwn = 3 * j - 2
    width = 1 + 2 * j

if(clone == "y"):
    printTruncAndTinsel_c(nbr_of_floors, stock)
else:
    printTruncAndTinsel(nbr_of_floors, stock)