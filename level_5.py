import time
width = 1
nbr_of_floors = 3
space_btwn = 0
l = 1
nbr_of_gaps = 10

def printleaves(nbr_of_gaps:int, width:int, j:int, space_btwn:int):
    for i in range(0, 4):
        nbr_of_gaps = nbr_of_gaps - j
        if(i == 0):
            if(width > 1):
                print((nbr_of_gaps - space_btwn - 1) * " " + "0" + space_btwn * " " + width * "*" + space_btwn * " " + "0")
            else:
                print(nbr_of_gaps * " " + width * "*")
        else:

            print(nbr_of_gaps * " " + width * "*")
        width = width + 2 * j
        time.sleep(0.3)
    return width

def printStar(star_center: str, l: int, nbr_of_gaps: int):
    if (l == 1 or l == 7):
        gap_btwn = 4
        print(nbr_of_gaps * " " + "*" + gap_btwn * " " + star_center + gap_btwn * " " + "*")
    elif (l == 2 or l == 6):
        gap_btwn = 2
        print(nbr_of_gaps * " " + gap_btwn * " " + "*" + gap_btwn * " " + star_center + gap_btwn * " " + "*")
    elif (l == 3 or l == 5):
        gap_btwn = 5
        print(nbr_of_gaps * " " + gap_btwn * " " + "*")
    else:
        print(nbr_of_gaps * " " + "* * * * * *")

while (l != 8):
    star_center = "*"
    if (l > 5):
        star_center = "|"
    printStar(star_center, l, nbr_of_gaps)
    time.sleep(0.3)
    l = l + 1
nbr_of_gaps = 16

for j in range(1, nbr_of_floors + 1):
    stock = printleaves(nbr_of_gaps, width, j, space_btwn)
    space_btwn = int(3 * j - 2)
    width = 1 + 2 * j

for k in range(1, 4):
    part_1 = " | | | | "
    part_2 = " 0 0 0 0 "
    if(k == 1):
        print(4 * " " + part_1 + 5 * "*" + part_1)
        time.sleep(0.3)
    elif(k == 2):
        print(4 * " " + part_2 + 5 * "*" + part_2)
        time.sleep(0.3)
    else:
        print(13 * " " + 5 * "*")